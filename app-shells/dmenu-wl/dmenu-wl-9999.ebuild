# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGIT_REPO_URI="https://github.com/nyyManni/dmenu-wayland"

inherit git-r3 meson

DESCRIPTION="wayland wlroot based dmenu"
HOMEPAGE="https://github.com/nyyManni/dmenu-wayland"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND="dev-libs/wayland
	dev-libs/wayland-protocols
	x11-libs/cairo
	x11-libs/pango
	x11-libs/libxkbcommon
	dev-libs/glib:2"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/fix-pedantic-c99.patch"
)
